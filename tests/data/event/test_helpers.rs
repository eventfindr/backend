use backend::data::event::helpers::{create_event_from_document, create_event_from_new_event};
use backend::type_defs::{Event, NewEvent};
use bson::doc;
use bson::oid::ObjectId;
use chrono::{DateTime, Utc};
use pretty_assertions::assert_eq;

#[test]
fn test_default_create_event_from_document() {
    let oid = ObjectId::new();
    let document = doc! {
        "_id": oid.clone(),
    };
    let now: DateTime<Utc> = Utc::now();
    let event = create_event_from_document(document);
    assert_eq!(
        event,
        Event {
            id: oid.clone(),
            event_id: "0".to_string(),
            name: "unknown".to_string(),
            summary: "unknown".to_string(),
            provider: "unknown".to_string(),
            image_url: "https://via.placeholder.com/500x250".to_string(),
            url: "#".to_string(),
            date: now,
            featured: false,
        }
    );
}

#[test]
fn test_create_event_from_new_event() {
    let oid = ObjectId::new();
    let now: DateTime<Utc> = Utc::now();
    let new_event = NewEvent {
        event_id: "1".to_string(),
        name: "Foo".to_string(),
        summary: "Bar".to_string(),
        provider: "TestProvider".to_string(),
        image_url: "https://example.com/foo.png".to_string(),
        url: "https://example.com/foo".to_string(),
        date: now,
        featured: false,
    };
    let event = create_event_from_new_event(new_event, &oid);
    assert_eq!(
        event,
        Event {
            id: oid.clone(),
            event_id: "1".to_string(),
            name: "Foo".to_string(),
            summary: "Bar".to_string(),
            provider: "TestProvider".to_string(),
            image_url: "https://example.com/foo.png".to_string(),
            url: "https://example.com/foo".to_string(),
            date: now,
            featured: false,
        }
    );
}
