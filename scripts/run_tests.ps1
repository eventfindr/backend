$env:CARGO_INCREMENTAL=0
$env:RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort"
$env:RUSTDOCFLAGS="-Cpanic=abort"

cargo +nightly-gnu build

cargo +nightly-gnu test --tests

Remove-Item Env:\CARGO_INCREMENTAL
Remove-Item Env:\RUSTFLAGS
Remove-Item Env:\RUSTDOCFLAGS
