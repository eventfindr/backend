FROM rust as builder
WORKDIR /build

COPY Cargo.toml Cargo.lock ./

RUN mkdir -p src/ && touch src/lib.rs && cargo build --release

COPY . .
RUN cargo build --release

FROM debian:stable-slim
WORKDIR /app

RUN apt-get update \
  && apt-get install -y --no-install-recommends lsb-release libssl-dev ca-certificates \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY --from=builder /build/target/release/backend .
COPY .env .

CMD ["./backend"]
