use crate::data::EventData;
use crate::db::get_db_handle;
use crate::providers::{get_providers, Provider};

use bson::doc;
use clokwerk::{ScheduleHandle, Scheduler, TimeUnits};
use log::{error, info};
use mongodb::options::UpdateModifications;
use std::thread;
use std::time::Duration;

fn reset_old_featured_events() {
    let db = get_db_handle();
    let collection = db.collection("events");
    info!("Resetting old featured events...");

    let query = doc! {
        "featured": true,
    };
    let update = doc! {
        "$set": {
            "featured": false,
        }
    };

    let res = collection.update_many(query, UpdateModifications::Document(update), None);
    match res {
        Ok(infos) => {
            info!("{} events modified.", infos.modified_count);
        }
        Err(err) => error!("{}", err),
    }
}

fn fetching_featured_events(provider: &dyn Provider) {
    let event_data = EventData::new();
    info!("Fetching featured events from {}...", provider.name());
    match provider.fetch_featured_events() {
        Ok(events) => {
            info!("Found {} events.", events.len());
            for event in events.iter() {
                let mut new_event = event.clone();
                new_event.featured = true;
                event_data.create_event(new_event);
            }
        }
        Err(err) => error!("{}", err),
    }
}

fn schedule() {
    info!("Fetching new events...");

    reset_old_featured_events();
    let providers = get_providers();
    for provider in providers.iter() {
        fetching_featured_events(provider.as_ref());
    }

    info!("Fetching finished.")
}

pub fn start_scheduler() -> ScheduleHandle {
    let mut scheduler = Scheduler::new();

    scheduler.every(1.days()).run(schedule);

    info!("Starting scheduler");
    thread::spawn(schedule);
    scheduler.watch_thread(Duration::from_millis(100))
}
