use cached::cached;
use log::info;
use mongodb::sync::{Client, Database};
use std::env;

cached! {
    DB_HANDLE;
    fn get_db_handle() -> Database = {
        let mongo_uri = env::var("MONGO_URI").unwrap();
        let mongo_database = env::var("MONGO_DATABASE").unwrap();
        info!("Connecting to {}...", mongo_uri);
        let client = Client::with_uri_str(&mongo_uri).unwrap();
        info!("Connected to {}.", mongo_uri);
        client.database(&mongo_database)
    }
}
