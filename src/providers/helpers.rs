use reqwest::cookie::Cookie;

pub fn get_csrf_token_from_cookies<'a>(
    cookies: impl Iterator<Item = Cookie<'a>>,
) -> Option<String> {
    for cookie in cookies {
        if cookie.name() == "csrftoken" {
            return Some(cookie.value().to_string());
        }
    }
    None
}
