use crate::providers::helpers::get_csrf_token_from_cookies;
use crate::providers::Provider;
use crate::type_defs::NewEvent;

use chrono::{DateTime, Utc};
use reqwest::blocking::{get, Client};
use reqwest::header::{COOKIE, REFERER, USER_AGENT};
use serde::Deserialize;
use serde_json::json;
use std::error::Error;

pub struct EventbriteProvider();

#[derive(Deserialize)]
struct EventImage {
    pub url: String,
}

impl Default for EventImage {
    fn default() -> Self {
        EventImage {
            url: "null".to_string(),
        }
    }
}

#[derive(Deserialize)]
struct EventbriteEvent {
    pub id: String,
    pub name: String,
    pub summary: String,
    #[serde(default)]
    pub image: EventImage,
    pub url: String,
    pub start_date: String,
    pub start_time: String,
}

#[derive(Deserialize)]
struct Results {
    pub results: Vec<EventbriteEvent>,
}

#[derive(Deserialize)]
struct FeaturedEvents {
    #[serde(rename = "flatBucket")]
    pub flat_bucket: Results,
}

#[derive(Deserialize)]
struct AutocompleteEvents {
    pub event: Vec<EventbriteEvent>,
}

#[derive(Deserialize)]
struct SearchResult {
    pub events: Results,
}

fn convert_eventbrite_event_to_new_event(
    event: &EventbriteEvent,
) -> Result<NewEvent, Box<dyn Error>> {
    let new_event = NewEvent {
        event_id: event.id.clone(),
        name: event.name.clone(),
        summary: event.summary.clone(),
        provider: "Eventbrite".to_string(),
        image_url: event.image.url.clone(),
        url: event.url.clone(),
        date: format!("{}T{}:00Z", event.start_date, event.start_time).parse::<DateTime<Utc>>()?,
        featured: false,
    };
    Ok(new_event)
}

fn fetch_eventbrite_featured_events() -> Result<Vec<NewEvent>, Box<dyn Error>> {
    let res: FeaturedEvents = get("https://www.eventbrite.com/fe/")?.json()?;
    let mut events: Vec<NewEvent> = Vec::new();

    for event in res.flat_bucket.results.iter() {
        events.push(convert_eventbrite_event_to_new_event(event)?);
    }
    Ok(events)
}

fn autocomplete_eventbrite_events(query: String) -> Result<Vec<NewEvent>, Box<dyn Error>> {
    let mut events: Vec<NewEvent> = Vec::new();
    let client = Client::new();
    let res: AutocompleteEvents = client
        .get("https://www.eventbrite.com/api/v3/destination/search/autocomplete/")
        .query(&[
            ("q", query.as_str()),
            ("completion_types", "event"),
            ("online_events_only", "true"),
            ("expand.destination_event", "image"),
        ])
        .send()?
        .json()?;

    for event in res.event.iter() {
        events.push(convert_eventbrite_event_to_new_event(event)?);
    }
    Ok(events)
}

fn get_eventbrite_csrf_token() -> Result<Option<String>, Box<dyn Error>> {
    let client = Client::new();
    let home_res = client
        .get("https://www.eventbrite.com/")
        .header(USER_AGENT, "EventFindr")
        .send()?;
    let cookies = home_res.cookies();
    Ok(get_csrf_token_from_cookies(cookies))
}

fn search_eventbrite_events_by_geolocation(
    latitude: f64,
    longitude: f64,
) -> Result<Vec<NewEvent>, Box<dyn Error>> {
    match get_eventbrite_csrf_token()? {
        Some(csrf_token) => {
            let mut events: Vec<NewEvent> = Vec::new();
            let client = Client::new();
            let precision = 0.3;
            let bbox = format!(
                "{},{},{},{}",
                longitude - precision,
                latitude - precision,
                longitude + precision,
                latitude + precision
            );
            let search_res: SearchResult = client
                .post("https://www.eventbrite.com/api/v3/destination/search/")
                .header("X-CSRFToken", csrf_token.clone())
                .header(REFERER, "https://www.eventbrite.com/")
                .header(COOKIE, format!("csrftoken={};", csrf_token))
                .json(&json!({
                    "event_search": {
                        "bbox": bbox,
                        "dates": "current_future",
                        "image": true,
                        "page_size": 30
                    },
                    "expand.destination_event": ["image"]
                }))
                .send()?
                .json()?;
            for event in search_res.events.results.iter() {
                events.push(convert_eventbrite_event_to_new_event(event)?);
            }
            Ok(events)
        }
        None => Err("Cannot get csrf token".into()),
    }
}

impl Provider for EventbriteProvider {
    fn name(&self) -> String {
        "Eventbrite".to_string()
    }

    fn fetch_featured_events(&self) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        fetch_eventbrite_featured_events()
    }

    fn autocomplete_events(&self, query: String) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        autocomplete_eventbrite_events(query)
    }

    fn search_events_by_geolocation(
        &self,
        latitude: f64,
        longitude: f64,
    ) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        search_eventbrite_events_by_geolocation(latitude, longitude)
    }
}
