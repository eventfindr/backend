use crate::providers::Provider;
use crate::type_defs::NewEvent;

use chrono::{DateTime, FixedOffset, Utc};
use reqwest::blocking::Client;
use reqwest::cookie::Cookie;
use reqwest::header::COOKIE;
use serde::Deserialize;
use serde_json::json;
use std::error::Error;

const GET_EVENTS_QUERY: &str = "
query eventShelf($lat: Float, $lon: Float, $first: Int) {\n
    upcomingEvents(search: {lat: $lat, lon: $lon}, input: {first: $first}) {\n
        edges {\n
            node {\n
            ...EventDetails\n
            }\n
        }\n
    }\n
}\n
fragment EventDetails on ChapstickEvent {\n
    id\n
    title\n
    dateTime\n
    description\n
    eventPhoto {\n
        ...PhotoDetails\n
    }\n
    link\n
}\n
fragment PhotoDetails on Image {\n
    id\n
    baseUrl\n
}\n
";

const SEARCH_EVENTS_QUERY: &str = "
query getSearchEvents($searchTerm: String, $lat: Float!, $lon: Float!, $first: Int) {\n
    searchEvents(search: {searchTerm: $searchTerm, lat: $lat, lon: $lon}, input: {first: $first}) {\n
        edges {\n
            node {\n
                id\n
                title\n
                dateTime\n
                description\n
                title\n
                eventPhoto {\n
                    id\n
                    baseUrl\n
                }\n
                link\n
            }\n
        }\n
    }\n
}\n
";

pub struct MeetupProvider();

#[derive(Deserialize)]
struct EventPhoto {
    pub id: String,
    #[serde(rename = "baseUrl")]
    pub base_url: String,
}

impl Default for EventPhoto {
    fn default() -> Self {
        EventPhoto {
            id: "null".to_string(),
            base_url: "null".to_string(),
        }
    }
}

#[derive(Deserialize)]
struct MeetupEvent {
    pub id: String,
    pub title: String,
    #[serde(rename = "dateTime")]
    pub date_time: String,
    pub description: Option<String>,
    #[serde(default)]
    #[serde(rename = "eventPhoto")]
    pub event_photo: Option<EventPhoto>,
    pub link: String,
}

#[derive(Deserialize)]
struct GetEventsResult {
    pub data: UpcomingEvents,
}

#[derive(Deserialize)]
struct SearchEventsResult {
    pub data: SearchEvents,
}

#[derive(Deserialize)]
struct SearchEvents {
    #[serde(rename = "searchEvents")]
    pub search_events: Edges,
}

#[derive(Deserialize)]
struct UpcomingEvents {
    #[serde(rename = "upcomingEvents")]
    pub upcoming_events: Edges,
}

#[derive(Deserialize)]
struct Edges {
    pub edges: Vec<Node>,
}

#[derive(Deserialize)]
struct Node {
    pub node: MeetupEvent,
}

pub fn get_meetup_browser_id_from_cookies<'a>(
    cookies: impl Iterator<Item = Cookie<'a>>,
) -> Option<String> {
    for cookie in cookies {
        if cookie.name() == "MEETUP_BROWSER_ID" {
            return Some(cookie.value().to_string());
        }
    }
    None
}

fn get_meetup_browser_id() -> Result<Option<String>, Box<dyn Error>> {
    let client = Client::new();
    let home_res = client.get("https://www.meetup.com/").send()?;
    let cookies = home_res.cookies();
    Ok(get_meetup_browser_id_from_cookies(cookies))
}

fn convert_meetup_event_to_new_event(event: &MeetupEvent) -> Result<NewEvent, Box<dyn Error>> {
    let mut image_url = "null".to_string();
    if let Some(event_photo) = &event.event_photo {
        if event_photo.base_url != "null" {
            image_url = format!("{}{}/952x536.jpg", event_photo.base_url, event_photo.id)
        }
    }
    let mut date_time = event.date_time.clone();
    if let Some(idx) = date_time.find('[') {
        date_time.truncate(idx);
    }

    let new_event = NewEvent {
        event_id: event.id.clone(),
        name: event.title.clone(),
        summary: event.description.clone().unwrap_or_else(|| "".to_string()),
        provider: "Meetup".to_string(),
        image_url,
        url: event.link.clone(),
        date: DateTime::from_utc(
            DateTime::<FixedOffset>::parse_from_str(date_time.as_str(), "%Y-%m-%dT%H:%M%.f%:z")?
                .naive_utc(),
            Utc,
        ),
        featured: false,
    };
    Ok(new_event)
}

fn fetch_meetup_events(latitude: f64, longitude: f64) -> Result<Vec<NewEvent>, Box<dyn Error>> {
    match get_meetup_browser_id()? {
        Some(meetup_browser_id) => {
            let client = Client::new();
            let body = json!({
                "operationName": "eventShelf",
                "variables": {
                    "lat": latitude,
                    "lon": longitude,
                    "first": 30
                },
                "query": GET_EVENTS_QUERY,
            });
            let res: GetEventsResult = client
                .post("https://api.meetup.com/gql")
                .json(&body)
                .header(COOKIE, format!("MEETUP_BROWSER_ID={};", meetup_browser_id))
                .send()?
                .json()?;

            let mut events: Vec<NewEvent> = Vec::new();

            for event in res.data.upcoming_events.edges.iter() {
                events.push(convert_meetup_event_to_new_event(&event.node)?);
            }
            Ok(events)
        }
        None => Err("Cannot get meetup browser id".into()),
    }
}

fn fetch_meetup_featured_events() -> Result<Vec<NewEvent>, Box<dyn Error>> {
    fetch_meetup_events(48.86, 2.34)
}

fn search_meetup_events_by_geolocation(
    latitude: f64,
    longitude: f64,
) -> Result<Vec<NewEvent>, Box<dyn Error>> {
    fetch_meetup_events(latitude, longitude)
}

fn autocomplete_meetup_events(query: String) -> Result<Vec<NewEvent>, Box<dyn Error>> {
    match get_meetup_browser_id()? {
        Some(meetup_browser_id) => {
            let client = Client::new();
            let body = json!({
                "operationName": "getSearchEvents",
                "variables": {
                    "searchTerm": query,
                    "lat": 48.86,
                    "lon": 2.34,
                    "first": 5
                },
                "query": SEARCH_EVENTS_QUERY,
            });
            let res: SearchEventsResult = client
                .post("https://api.meetup.com/gql")
                .json(&body)
                .header(COOKIE, format!("MEETUP_BROWSER_ID={};", meetup_browser_id))
                .send()?
                .json()?;

            let mut events: Vec<NewEvent> = Vec::new();

            for event in res.data.search_events.edges.iter() {
                events.push(convert_meetup_event_to_new_event(&event.node)?);
            }
            Ok(events)
        }
        None => Err("Cannot get meetup browser id".into()),
    }
}

impl Provider for MeetupProvider {
    fn name(&self) -> String {
        "Meetup".to_string()
    }

    fn fetch_featured_events(&self) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        fetch_meetup_featured_events()
    }

    fn autocomplete_events(&self, query: String) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        autocomplete_meetup_events(query)
    }

    fn search_events_by_geolocation(
        &self,
        latitude: f64,
        longitude: f64,
    ) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        search_meetup_events_by_geolocation(latitude, longitude)
    }
}
