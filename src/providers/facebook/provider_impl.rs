use crate::providers::Provider;
use crate::type_defs::NewEvent;

use chrono::{DateTime, FixedOffset, Utc};
use reqwest::blocking::Client;
use reqwest::header::{ACCEPT_LANGUAGE, USER_AGENT};
use serde::Deserialize;
use std::error::Error;

pub struct FacebookProvider();

#[derive(Deserialize)]
struct EventImage {
    pub uri: String,
}

impl Default for EventImage {
    fn default() -> Self {
        EventImage {
            uri: "null".to_string(),
        }
    }
}

#[derive(Deserialize)]
struct FacebookEvent {
    pub id: i64,
    pub title: String,
    pub description: String,
    #[serde(default)]
    #[serde(rename = "coverPhoto")]
    pub cover_photo: EventImage,
    pub day: String,
    pub month: String,
    #[serde(rename = "dateAndTime")]
    pub date_and_time: String,
}

#[derive(Deserialize)]
struct FeaturedEvents {
    pub payload: Results,
}

#[derive(Deserialize)]
struct Results {
    pub results: Vec<Events>,
}

#[derive(Deserialize)]
struct Events {
    pub events: Vec<FacebookEvent>,
}

fn crop_letters(s: &mut String, pos: usize) {
    match s.char_indices().nth(pos) {
        Some((pos, _)) => {
            s.drain(..pos);
        }
        None => {
            s.clear();
        }
    }
}

fn convert_facebook_event_to_new_event(event: &FacebookEvent) -> Result<NewEvent, Box<dyn Error>> {
    let mut fb_datetime = event.date_and_time.clone();
    if fb_datetime.contains(" - ") {
        fb_datetime = "1:00 PM UTC+02".to_string();
    } else {
        crop_letters(&mut fb_datetime, 4);
    }
    let datetime = format!(
        "{} {} {} {}",
        event.day,
        event.month,
        Utc::now().format("%Y"),
        fb_datetime
    );
    let new_event = NewEvent {
        event_id: event.id.to_string(),
        name: event.title.clone(),
        summary: event.description.clone(),
        provider: "Facebook".to_string(),
        image_url: event.cover_photo.uri.clone(),
        url: format!("https://www.facebook.com/events/{}/", event.id.clone()),
        date: DateTime::from_utc(
            DateTime::<FixedOffset>::parse_from_str(datetime.as_str(), "%d %b %Y %l:%M %p UTC%#z")?
                .naive_utc(),
            Utc,
        ),
        featured: false,
    };
    Ok(new_event)
}

fn fetch_facebook_featured_events() -> Result<Vec<NewEvent>, Box<dyn Error>> {
    let client = Client::new();
    let params = [
        ("suggestion_token", "{\"city\":\"110774245616525\"}"),
        ("__a", "1"),
    ];
    let res = client
        .post("https://www.facebook.com/events/discover/query/")
        .header(USER_AGENT, "EventFindr")
        .header(ACCEPT_LANGUAGE, "en;")
        .form(&params)
        .send()?
        .text()?;

    let clean_text = res.replace("for (;;);", "");
    let json: FeaturedEvents = serde_json::from_str(clean_text.as_str())?;

    let mut events: Vec<NewEvent> = Vec::new();

    if !json.payload.results.is_empty() {
        for event in json.payload.results[0].events.iter() {
            events.push(convert_facebook_event_to_new_event(event)?);
        }
    }
    Ok(events)
}

impl Provider for FacebookProvider {
    fn name(&self) -> String {
        "Facebook".to_string()
    }

    fn fetch_featured_events(&self) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        fetch_facebook_featured_events()
    }

    fn autocomplete_events(&self, _query: String) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        Ok(Vec::new())
    }

    fn search_events_by_geolocation(
        &self,
        _latitude: f64,
        _longitude: f64,
    ) -> Result<Vec<NewEvent>, Box<dyn Error>> {
        Ok(Vec::new())
    }
}
