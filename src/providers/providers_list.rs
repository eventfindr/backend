use super::eventbrite;
use super::facebook;
use super::meetup;
use crate::providers::Provider;

pub fn get_providers() -> Vec<Box<dyn Provider>> {
    let mut providers: Vec<Box<dyn Provider>> = Vec::new();

    providers.push(Box::new(eventbrite::EventbriteProvider()));
    providers.push(Box::new(facebook::FacebookProvider()));
    providers.push(Box::new(meetup::MeetupProvider()));

    providers
}
