use crate::type_defs::NewEvent;
use std::error::Error;

pub trait Provider {
    fn name(&self) -> String;
    fn fetch_featured_events(&self) -> Result<Vec<NewEvent>, Box<dyn Error>>;
    fn autocomplete_events(&self, query: String) -> Result<Vec<NewEvent>, Box<dyn Error>>;
    fn search_events_by_geolocation(
        &self,
        latitude: f64,
        longitude: f64,
    ) -> Result<Vec<NewEvent>, Box<dyn Error>>;
}
