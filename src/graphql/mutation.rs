use super::schema::Context;
use crate::type_defs::{Event, NewEvent};
use juniper::FieldResult;

pub struct Mutation;

#[juniper::graphql_object(Context = Context)]
impl Mutation {
    pub async fn create_event(ctx: &Context, data: NewEvent) -> FieldResult<Event> {
        Ok(ctx.event_data.create_event(data))
    }
}
