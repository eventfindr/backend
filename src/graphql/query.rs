pub struct Query;
use super::schema::Context;
use crate::type_defs::{Event, EventOrderBy, ProviderStats};
use bson::oid::ObjectId;
use chrono::{DateTime, Utc};
use juniper::FieldResult;

#[juniper::graphql_object(Context = Context)]
impl Query {
    fn event_by_id(context: &Context, id: String) -> FieldResult<Event> {
        Ok(context.event_data.event_by_id(ObjectId::with_string(&id)?))
    }

    fn events(
        context: &Context,
        page: Option<i32>,
        order_by: Option<EventOrderBy>,
        featured_only: Option<bool>,
        provider_filter: Option<Vec<String>>,
        start_date: Option<DateTime<Utc>>,
        end_date: Option<DateTime<Utc>>,
    ) -> FieldResult<Vec<Event>> {
        Ok(context.event_data.get_all_events(
            page,
            order_by,
            featured_only,
            provider_filter,
            start_date,
            end_date,
        ))
    }

    fn autocomplete_events(context: &Context, query: String) -> FieldResult<Vec<Event>> {
        Ok(context.event_data.autocomplete_events(query))
    }

    fn search_events_by_geolocation(
        context: &Context,
        latitude: f64,
        longitude: f64,
    ) -> FieldResult<Vec<Event>> {
        Ok(context
            .event_data
            .search_events_by_geolocation(latitude, longitude))
    }

    fn providers_stats(
        context: &Context,
        without_total: Option<bool>,
    ) -> FieldResult<Vec<ProviderStats>> {
        Ok(context
            .provider_stats_data
            .get_provider_stats(without_total))
    }
}
