use crate::data::EventData;
use crate::data::ProviderStatsData;
use crate::graphql::schema::{Context, Schema};
use actix_web::web::block;
use actix_web::{web, Error, HttpResponse};
use juniper::http::{playground::playground_source, GraphQLRequest};
use std::sync::Arc;

pub async fn graphql(
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> Result<HttpResponse, Error> {
    // Context setup
    let event_data = EventData::new();
    let provider_stats_data = ProviderStatsData::new();
    let ctx = Context::new(event_data, provider_stats_data);

    // Execute
    let json = block(move || {
        let res = data.execute_sync(&st, &ctx);
        serde_json::to_string(&res)
    })
    .await?;

    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(json))
}

pub fn playground() -> HttpResponse {
    let html = playground_source("/graphql", None);
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}
