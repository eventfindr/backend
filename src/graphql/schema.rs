use super::{mutation::Mutation, query::Query};
use crate::data::EventData;
use crate::data::ProviderStatsData;

use juniper::EmptySubscription;

#[derive(Clone)]
pub struct Context {
    pub event_data: EventData,
    pub provider_stats_data: ProviderStatsData,
}

impl juniper::Context for Context {}

impl Context {
    pub fn new(event_data: EventData, provider_stats_data: ProviderStatsData) -> Self {
        Self {
            event_data,
            provider_stats_data,
        }
    }
}

pub type Schema = juniper::RootNode<'static, Query, Mutation, EmptySubscription<Context>>;

pub fn create_schema() -> Schema {
    Schema::new(Query {}, Mutation {}, EmptySubscription::<Context>::new())
}
