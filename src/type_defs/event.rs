use crate::graphql::schema::Context;

use bson::oid::ObjectId;
use chrono::DateTime;
use chrono::Utc;

#[derive(juniper::GraphQLEnum)]
pub enum EventOrderBy {
    DateAsc,
    DateDesc,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Event {
    pub id: ObjectId,
    pub event_id: String,
    pub name: String,
    pub summary: String,
    pub provider: String,
    pub image_url: String,
    pub url: String,
    pub date: DateTime<Utc>,
    pub featured: bool,
}

#[derive(juniper::GraphQLInputObject, Debug, Clone)]
#[graphql(name = "NewEvent", description = "A creating an event!")]
pub struct NewEvent {
    pub event_id: String,
    pub name: String,
    pub summary: String,
    pub provider: String,
    pub image_url: String,
    pub url: String,
    pub date: DateTime<Utc>,
    pub featured: bool,
}

#[juniper::graphql_object(Context = Context)]
impl Event {
    pub fn id(&self) -> ObjectId {
        self.id.clone()
    }

    pub fn event_id(&self) -> &str {
        self.event_id.as_str()
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn summary(&self) -> &str {
        self.summary.as_str()
    }

    pub fn provider(&self) -> &str {
        self.provider.as_str()
    }

    pub fn image_url(&self) -> &str {
        self.image_url.as_str()
    }

    pub fn url(&self) -> &str {
        self.url.as_str()
    }

    pub fn date(&self) -> DateTime<Utc> {
        self.date
    }

    pub fn featured(&self) -> bool {
        self.featured
    }
}
