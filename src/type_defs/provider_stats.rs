use crate::graphql::schema::Context;

#[derive(Debug, Clone)]
pub struct ProviderStats {
    pub provider: String,
    pub events_number: i32,
}

#[juniper::graphql_object(Context = Context)]
impl ProviderStats {
    pub fn provider(&self) -> &str {
        self.provider.as_str()
    }

    pub fn events_number(&self) -> i32 {
        self.events_number
    }
}
