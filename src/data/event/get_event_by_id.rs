use crate::db::get_db_handle;
use crate::type_defs::Event;

use super::helpers::create_event_from_document;

use async_trait::async_trait;
use bson::doc;
use bson::oid::ObjectId;
use dataloader::cached::Loader;
use dataloader::BatchFn;
use std::collections::HashMap;

pub fn get_event_by_ids(hashmap: &mut HashMap<ObjectId, Event>, ids: Vec<ObjectId>) {
    let db = get_db_handle();
    let collection = db.collection("events");
    for id in ids {
        let document = collection
            .find_one(doc! { "_id": id }, None)
            .unwrap()
            .unwrap();
        let event = create_event_from_document(document);
        hashmap.insert(event.id.clone(), event);
    }
}

pub struct EventBatcher;

#[async_trait]
impl BatchFn<ObjectId, Event> for EventBatcher {
    async fn load(&self, keys: &[ObjectId]) -> HashMap<ObjectId, Event> {
        let mut event_hashmap = HashMap::new();
        get_event_by_ids(&mut event_hashmap, keys.to_vec());
        event_hashmap
    }
}

pub type EventLoader = Loader<ObjectId, Event, EventBatcher>;

pub fn get_loader() -> EventLoader {
    Loader::new(EventBatcher).with_yield_count(100)
}
