use crate::type_defs::{Event, EventOrderBy, NewEvent};
use bson::oid::ObjectId;
use chrono::{DateTime, Utc};
use tokio::runtime::Runtime;

pub mod autocomplete_events;
pub mod create_event;
pub mod get_all_events;
pub mod get_event_by_id;
pub mod search_events_by_geolocation;

pub mod helpers;

#[derive(Clone)]
pub struct EventData {
    event_by_id: get_event_by_id::EventLoader,
}

impl EventData {
    pub fn new() -> EventData {
        EventData {
            event_by_id: get_event_by_id::get_loader(),
        }
    }

    pub fn event_by_id(&self, id: ObjectId) -> Event {
        Runtime::new().unwrap().block_on(self.event_by_id.load(id))
    }

    pub fn create_event(&self, data: NewEvent) -> Event {
        create_event::create_event(data)
    }

    pub fn get_all_events(
        &self,
        page: Option<i32>,
        order_by: Option<EventOrderBy>,
        featured_only: Option<bool>,
        provider_filter: Option<Vec<String>>,
        start_date: Option<DateTime<Utc>>,
        end_date: Option<DateTime<Utc>>,
    ) -> Vec<Event> {
        get_all_events::get_all_events(
            page,
            order_by,
            featured_only,
            provider_filter,
            start_date,
            end_date,
        )
    }

    pub fn autocomplete_events(&self, query: String) -> Vec<Event> {
        autocomplete_events::autocomplete_events(query)
    }

    pub fn search_events_by_geolocation(&self, latitude: f64, longitude: f64) -> Vec<Event> {
        search_events_by_geolocation::search_events_by_geolocation(latitude, longitude)
    }
}
