use crate::db::get_db_handle;
use crate::type_defs::{Event, EventOrderBy};

use super::helpers::create_event_from_document;

use bson::doc;
use bson::document::Document;
use chrono::{DateTime, Utc};
use mongodb::options::FindOptions;

fn get_filter(
    featured_only: Option<bool>,
    provider_filter: Option<Vec<String>>,
    start_date: Option<DateTime<Utc>>,
    end_date: Option<DateTime<Utc>>,
) -> Document {
    let mut document = doc! {};
    if let Some(featured_only) = featured_only {
        if featured_only {
            document.insert("featured", true);
        }
    }
    if let Some(provider_filter) = provider_filter {
        document.insert(
            "provider",
            doc! {
                "$in": provider_filter,
            },
        );
    }
    if let (Some(start_date), Some(end_date)) = (start_date, end_date) {
        document.insert(
            "date",
            doc! {
                "$gte": start_date,
                "$lte": end_date,
            },
        );
    } else if let Some(start_date) = start_date {
        document.insert(
            "date",
            doc! {
                "$gte": start_date,
            },
        );
    } else if let Some(end_date) = end_date {
        document.insert(
            "date",
            doc! {
                "$lte": end_date,
            },
        );
    }
    document
}

fn get_find_options(page: Option<i32>, order_by: Option<EventOrderBy>) -> Option<FindOptions> {
    let find_options = FindOptions::builder();
    let first_options;
    match page {
        Some(page) => {
            let items_per_page: i64 = 10;
            first_options = find_options
                .skip(Some(i64::from(page) * items_per_page))
                .limit(Some(items_per_page));
        }
        None => {
            first_options = find_options.skip(None).limit(None);
        }
    }
    let second_options;
    match order_by {
        Some(order) => {
            let document = match order {
                EventOrderBy::DateAsc => doc! { "date": 1 },
                EventOrderBy::DateDesc => doc! { "date": -1 },
            };
            second_options = first_options.sort(document)
        }
        None => second_options = first_options.sort(None),
    }
    Some(second_options.build())
}

pub fn get_all_events(
    page: Option<i32>,
    order_by: Option<EventOrderBy>,
    featured_only: Option<bool>,
    provider_filter: Option<Vec<String>>,
    start_date: Option<DateTime<Utc>>,
    end_date: Option<DateTime<Utc>>,
) -> Vec<Event> {
    let mut vec = Vec::new();
    let db = get_db_handle();
    let collection = db.collection("events");
    let filter = get_filter(featured_only, provider_filter, start_date, end_date);
    let find_options = get_find_options(page, order_by);
    let cursor = collection.find(filter, find_options).unwrap();

    for result in cursor {
        let document = result.unwrap();
        let event = create_event_from_document(document);
        vec.push(event);
    }
    vec
}
