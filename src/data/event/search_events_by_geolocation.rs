use crate::data::EventData;
use crate::providers::get_providers;
use crate::type_defs::Event;

use log::error;

pub fn search_events_by_geolocation(latitude: f64, longitude: f64) -> Vec<Event> {
    let providers = get_providers();
    let event_data = EventData::new();
    let mut events_vec: Vec<Event> = Vec::new();
    for provider in providers.iter() {
        match provider.search_events_by_geolocation(latitude, longitude) {
            Ok(new_events) => {
                for new_event in new_events.iter() {
                    let event = event_data.create_event(new_event.clone());
                    events_vec.push(event);
                }
            }
            Err(err) => error!("{}", err),
        }
    }
    events_vec
}
