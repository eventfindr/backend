use crate::type_defs::{Event, NewEvent};

use bson::document::Document;
use bson::oid::ObjectId;
use bson::Bson;
use chrono::Utc;

pub fn create_event_from_document(document: Document) -> Event {
    let id = document.get("_id").and_then(Bson::as_object_id).unwrap();
    let event_id = document
        .get("event_id")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "0");
    let name = document
        .get("name")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "unknown");
    let summary = document
        .get("summary")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "unknown");
    let provider = document
        .get("provider")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "unknown");
    let image_url = document
        .get("image_url")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "https://via.placeholder.com/500x250");
    let url = document
        .get("url")
        .and_then(Bson::as_str)
        .unwrap_or_else(|| "#");
    let now = Utc::now();
    let date = document
        .get("date")
        .and_then(Bson::as_datetime)
        .unwrap_or_else(|| &now);
    let featured = document
        .get("featured")
        .and_then(Bson::as_bool)
        .unwrap_or_else(|| false);
    Event {
        id: id.clone(),
        event_id: String::from(event_id),
        name: String::from(name),
        summary: String::from(summary),
        provider: String::from(provider),
        image_url: String::from(image_url),
        url: String::from(url),
        date: *date,
        featured,
    }
}

pub fn create_event_from_new_event(new_event: NewEvent, id: &ObjectId) -> Event {
    Event {
        id: id.clone(),
        event_id: new_event.event_id,
        name: new_event.name,
        summary: new_event.summary,
        provider: new_event.provider,
        image_url: new_event.image_url,
        url: new_event.url,
        date: new_event.date,
        featured: new_event.featured,
    }
}
