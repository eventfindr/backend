use crate::db::get_db_handle;
use crate::type_defs::{Event, NewEvent};

use super::helpers::create_event_from_new_event;

use bson::doc;
use bson::oid::ObjectId;
use log::error;

fn update_event(data: NewEvent, id: &ObjectId) -> Event {
    let db = get_db_handle();
    let collection = db.collection("events");

    let query = doc! {
        "_id": id,
    };
    let update = doc! {
        "$set": {
            "name": data.name.clone(),
            "summary": data.summary.clone(),
            "image_url": data.image_url.clone(),
            "url": data.url.clone(),
            "date": data.date,
            "featured": data.featured,
        }
    };

    let res = collection.update_one(query, update, None);
    match res {
        Ok(_) => (),
        Err(err) => error!("{}", err),
    }
    create_event_from_new_event(data, id)
}

pub fn create_event(data: NewEvent) -> Event {
    let db = get_db_handle();
    let collection = db.collection("events");

    let filter = doc! {
        "event_id": data.event_id.clone(),
        "provider": data.provider.clone(),
    };
    let result = collection.find_one(filter, None).unwrap();
    match result {
        Some(document) => update_event(data, document.get_object_id("_id").unwrap()),
        None => {
            let res = collection
                .insert_one(
                    doc! {
                        "event_id": data.event_id.clone(),
                        "name": data.name.clone(),
                        "summary": data.summary.clone(),
                        "provider": data.provider.clone(),
                        "image_url": data.image_url.clone(),
                        "url": data.url.clone(),
                        "date": data.date,
                        "featured": data.featured,
                    },
                    None,
                )
                .unwrap();
            let id = res.inserted_id.as_object_id().unwrap();
            create_event_from_new_event(data, id)
        }
    }
}
