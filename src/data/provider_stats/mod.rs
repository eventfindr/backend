use crate::type_defs::ProviderStats;

pub mod get_providers_stats;

#[derive(Clone, Default)]
pub struct ProviderStatsData {}

impl ProviderStatsData {
    pub fn new() -> ProviderStatsData {
        ProviderStatsData {}
    }

    pub fn get_provider_stats(&self, without_total: Option<bool>) -> Vec<ProviderStats> {
        get_providers_stats::get_providers_stats(without_total)
    }
}
