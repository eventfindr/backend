use crate::db::get_db_handle;
use crate::providers::get_providers;
use crate::type_defs::ProviderStats;

use bson::doc;
use log::error;
use mongodb::sync::Collection;
use std::convert::TryFrom;

fn add_total_providers_stats(collection: Collection, stats: &mut Vec<ProviderStats>) {
    let res = collection.count_documents(None, None);
    match res {
        Ok(count) => stats.push(ProviderStats {
            provider: "Total".to_string(),
            events_number: i32::try_from(count).unwrap(),
        }),
        Err(err) => error!("{}", err),
    }
}

pub fn get_providers_stats(without_total: Option<bool>) -> Vec<ProviderStats> {
    let mut vec = Vec::new();
    let db = get_db_handle();
    let collection = db.collection("events");
    let providers = get_providers();

    for provider in providers.iter() {
        let filter = doc! {
            "provider": provider.name(),
        };
        let res = collection.count_documents(filter, None);
        match res {
            Ok(count) => vec.push(ProviderStats {
                provider: provider.name(),
                events_number: i32::try_from(count).unwrap(),
            }),
            Err(err) => error!("{}", err),
        }
    }
    match without_total {
        Some(without_total) => {
            if !without_total {
                add_total_providers_stats(collection, &mut vec)
            }
        }
        None => add_total_providers_stats(collection, &mut vec),
    }
    vec
}
