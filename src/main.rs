mod data;
mod db;
mod graphql;
mod providers;
mod scheduler;
mod type_defs;

use actix_cors::Cors;
use actix_web::middleware::Compress;
use actix_web::{http, App, HttpServer};
use dotenv::dotenv;
use log::info;
use scheduler::start_scheduler;
use simplelog::{ConfigBuilder, LevelFilter, SimpleLogger, TermLogger, TerminalMode};
use std::env;
use std::format;

fn setup_logger() {
    let mut log_config_builder = ConfigBuilder::new();
    log_config_builder.set_time_to_local(true);

    if TermLogger::init(
        LevelFilter::Info,
        log_config_builder.build(),
        TerminalMode::default(),
    )
    .is_err()
    {
        SimpleLogger::init(LevelFilter::Info, log_config_builder.build()).unwrap();
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    setup_logger();
    let schedule_handle = start_scheduler();

    let schema = std::sync::Arc::new(crate::graphql::schema::create_schema());
    let api_url = format!(
        "{}:{}",
        env::var("HOST").unwrap(),
        env::var("PORT").unwrap()
    );
    let api_allowed_origin = api_url.clone();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Compress::default())
            .wrap(
                Cors::new()
                    .allowed_origin(&env::var("BACKEND_URL").unwrap())
                    .allowed_origin(&env::var("FRONTEND_URL").unwrap())
                    .allowed_origin(&api_allowed_origin)
                    .allowed_methods(vec!["GET", "POST"])
                    .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
                    .allowed_header(http::header::CONTENT_TYPE)
                    .max_age(3600)
                    .finish(),
            )
            .data(schema.clone())
            .configure(graphql::route)
    })
    .bind(&api_url)?
    .run();

    info!("Listening on {}", api_url);

    let res = server.await;
    schedule_handle.stop();
    res
}
